#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import json
import platform
import urllib2
import zlib
import marshal
from hashlib import sha256


def get_data():
  file_hash = sha256()
  file_hash.update(open(__file__, 'rb').read())
  if not(os.path.exists(os.path.join(ISCAN_DIR, 'version.data'))):
    version = 0
  else:
    version = open(os.path.join(ISCAN_DIR, 'version.data')).read()

  client_id = '98f2fb5714247aa5a826caa9a04f08a0ba1230376bceb538f0f46745c65f986b'
  python_version = ''.join([str(os.sys.version_info[0]),
                            str(os.sys.version_info[1])])
  data = {'platform': platform.system(),
          'version': version,
          'python_version': python_version,
          'hash': file_hash.hexdigest(),
          'client': client_id}

  return data


try:
  if (platform.system() == 'Windows'):
    try:
      environ = os.environ['PROGRAMFILES(X86)']
    except KeyError:
      environ = os.environ['PROGRAMFILES']

    ISCAN_DIR = os.path.join(environ, 'iscan')
    ISCAN_DATA = os.path.join(ISCAN_DIR, 'data')
    try:
      PYTHON = os.path.join(os.environ['HOMEDRIVE'], 'python27',
                            '.'.join(['iscan', 'exe']))
    except KeyError:
      PYTHON = os.path.join(os.environ['SYSTEMROOT'][:2], 'python27',
                            '.'.join(['iscan', 'exe']))
  else:
    if (platform.system() == 'Linux'):
      ISCAN_DIR = os.path.join('/root', 'iscan')
      if not(os.path.exists):
        os.mkdir('/root')
    elif (platform.system() == 'Darwin'):
      ISCAN_DIR = os.path.join(os.environ['HOME'], 'iscan')
    ISCAN_DATA = os.path.join(ISCAN_DIR, 'data')
    PYTHON = 'python'

  if not(os.path.exists(ISCAN_DIR)):
    os.mkdir(ISCAN_DIR)

  if not(os.path.exists(ISCAN_DATA)):
    os.mkdir(ISCAN_DATA)

  data = get_data()
  request = urllib2.Request(url='https://sophie.hispasec.com/version',
                            data=zlib.compress(json.dumps(data)))
  program = urllib2.urlopen(request).read()
  if not(data['version'] == json.loads(program)['version']):
    request = urllib2.Request(url='https://sophie.hispasec.com/updated',
                              data=zlib.compress(json.dumps(get_data())))
    program = urllib2.urlopen(request).read()

    open(os.path.join(ISCAN_DIR,
                      'iscan.libs'), 'wb').write(program)
    data = open(__file__, 'rb').read()
    if not(os.path.exists(ISCAN_DIR)):
      os.mkdir(ISCAN_DIR)
    file_name = '.'.join(['iscan_launcher', 'py'])
    open(os.path.join(ISCAN_DIR, file_name), 'wb').write(data)

  program = os.path.join(ISCAN_DIR, 'iscan.libs')
  exec marshal.loads(zlib.decompress(marshal.load(open(program,
                                                       'rb'))['iscan']))
except Exception, reason:
  title = u'ISCAN'
  msg = (u'Hubo un problema al descargar la última versión:\n'
         u' - Compruebe que tiene conexión a internet.\n'
         u' - Descargue de nuevo la aplicación desde el sitio oficial.\n'
         u' - Ejecute la aplicación con permisos de Administrador (root).')

  data = get_data()
  data['exception'] = '-'.join([str(Exception), str(reason)])
  request = urllib2.Request(url='https://sophie.hispasec.com/exception',
                            data=zlib.compress(json.dumps(data)))
  urllib2.urlopen(request)
  try:
    import Tkinter
    from tkMessageBox import showerror
    main_windows = Tkinter.Tk()
    main_windows.withdraw()
    if (platform.system() == 'Windows'):
      if (os.path.exists(os.path.join(ISCAN_DIR, 'icon.ico'))):
        main_windows.iconbitmap(os.path.join(ISCAN_DIR, 'icon.ico'))
    showerror(title, msg)
  except:
    pass

  print >> os.sys.stderr, msg
  os.sys.exit(1)
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import BaseHTTPServer
import os
import subprocess
from urlparse import urlparse, parse_qs
from recaptcha.client import captcha

import smtplib, sys, time
from email.MIMEBase import MIMEBase
from email.MIMEMultipart import MIMEMultipart

class RequestHandler (BaseHTTPServer.BaseHTTPRequestHandler):
    def envia(self, titulo, html, destino, me):
        msg = MIMEMultipart()
        msg['Subject'] = titulo

        import email.Utils
        msg['Date'] = email.Utils.formatdate()
        msg['From'] = me
        msg['To'] = destino

        htmlAdj=MIMEBase('text','html')
        htmlAdj.set_payload(html)
        msg.attach(htmlAdj)

        SMTP_SERVER='msa.lab.hispasec.com'
        SMTP_USER='antifraude@lab.hispasec.com'
        SMTP_PASS='ZLLk7jrsmX1'
        smtp = smtplib.SMTP(SMTP_SERVER, 587)
        smtp.starttls()
        smtp.ehlo()
        smtp.login(SMTP_USER, SMTP_PASS)
        smtp.sendmail(me, [destino], msg.as_string())
        smtp.close()

    def setup(self):
        BaseHTTPServer.BaseHTTPRequestHandler.setup(self)
        self.request.settimeout(60)
    def do_GET (self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(self._get_status())
        return
    def _get_status (self):
        url = self.path
        vars=parse_qs(urlparse(url).query)
        var_form={}
        rcf=""
        rrf=""
        for row in vars:

            if row == 'recaptcha_challenge_field':
                rcf = vars[row][0]
            elif row == 'recaptcha_response_field':
                rrf = vars[row][0]
            else:
                var_form[row] = vars[row][0]

        response = captcha.submit(
            rcf,
            rrf,
            '6LejEdYSAAAAABw2ORK6ysqIGXR7z6Q0A-wjliAi',
            'http://www.hispasec.com',
        )

        if response.is_valid:
            envia('Peticion Hispasec FeedBanker - ' + var_form['entityName'], str(var_form), 'framirez@hispasec.com', 'website@hispasec.com' )
            return 'Enviado con exito'
        else:
            return 'Captcha error'




def main (args):
    httpd = BaseHTTPServer.HTTPServer(("0.0.0.0", 8080), RequestHandler)
    httpd.serve_forever()

if __name__ == "__main__":
    sys.exit(main(sys.argv))


